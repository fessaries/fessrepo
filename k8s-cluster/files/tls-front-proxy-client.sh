cfssl gencert \
  -ca=tmp/front-proxy-ca.pem \
  -ca-key=tmp/front-proxy-ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  front-proxy-client-csr.json | cfssljson -bare tmp/front-proxy-client