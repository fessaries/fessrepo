cfssl gencert \
  -ca=tmp/ca.pem \
  -ca-key=tmp/ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare tmp/kube-scheduler
