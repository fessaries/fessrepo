#! /bin/sh

find $ARRVOLPATH/media -maxdepth 1 -mmin +60 \
        -not -path $ARRVOLPATH/media/movies \
        -not -path $ARRVOLPATH/media/tv \
        -not -path $ARRVOLPATH/media/incomplete \
        -not -path $ARRVOLPATH/media \
        -execdir rm -r {} ';'
