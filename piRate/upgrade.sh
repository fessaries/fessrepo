#!/bin/bash

cd "${0%/*}"
docker-compose stop
docker-compose pull
docker-compose up -d
