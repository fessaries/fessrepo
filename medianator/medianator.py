#!/usr/bin/env python3

import requests
import argparse
import shutil

parser = argparse.ArgumentParser(
    description='Deletes old sonarr/radarr media files if media volume is below watermark.')
parser.add_argument("-p", "--path", help="media file path to check free disk",required=True,)
parser.add_argument("-sk", "--sonarrkey", help="sonarr API key")
parser.add_argument("-rk", "--radarrkey", help="radarr API key")
parser.add_argument("-sh", "--sonarrhost", help="sonarr host")
parser.add_argument("-rh", "--radarrhost", help="radarr host")
parser.add_argument("-sp", "--sonarrport", help="sonarr port", default='8989')
parser.add_argument("-rp", "--radarrport", help="radarr port", default='7878')
parser.add_argument("-d", "--diskwm", help="low disk watermark in GiB", type=int, default=20)
parser.add_argument("-n", "--noop", help="no-op dry run", action='store_true')
args = parser.parse_args()

if (args.sonarrhost != None and args.sonarrkey == None):
    parser.error('The --sonarrhost argument requires the --sonarrkey argument.')

if (args.radarrhost != None and args.radarrkey == None):
    parser.error('The --radarrhost argument requires the --radarrkey argument.')

lowDiskMark = args.diskwm
sonApiKey = args.sonarrkey
radApiKey = args.radarrkey
sonHost = args.sonarrhost
radHost = args.radarrhost
sonPort = args.sonarrport
radPort = args.radarrport
noop = args.noop

def df_media_path():
    mediaPath = args.path
    try:
        diskTotal, diskUsed, diskFree = shutil.disk_usage(mediaPath)
    except FileNotFoundError:
        print('Media path not found at', mediaPath)
        exit(1)
    return diskFree // 1024 ** 3

def delete_old_tv(tvCandidates):
    payload = {'apiKey': sonApiKey}
    for i in tvCandidates:
        print(i['relativePath'],'- Added:',i['dateAdded'])
        if noop == False:
            r = requests.delete("{}/episodefile/{}".format(sonarrApiUrl,i['id']), params=payload)
            r.raise_for_status()

def get_tv_candidates(numberOfCandidates):
    payload = {'apiKey': sonApiKey}
    seriesData = requests.get('{}/series'.format(sonarrApiUrl), params=payload)
    seriesData.raise_for_status()
    seriesDataJson = seriesData.json()
    allEpisodeFiles = []

    for i in seriesDataJson:
        if i['statistics']['episodeFileCount'] > 0:
            payload['seriesId']=i['id']         
            episodeData = requests.get('{}/episodefile'.format(sonarrApiUrl), params=payload)
            episodeDataJson = episodeData.json()
            allEpisodeFiles.extend(episodeDataJson)

    sortedEpisodeFiles = sorted(allEpisodeFiles, key = lambda i: i['dateAdded'])
    return sortedEpisodeFiles[:numberOfCandidates]

def delete_old_movies(movieCandidates):
    payload = {'apiKey': radApiKey, 'deleteFiles': 'true'}
    for i in movieCandidates:
        print (i['title'],'- Added:',i['movieFile']['dateAdded'])
        if noop == False:
            r = requests.delete("{}/movie/{}".format(radarrApiUrl,i['id']), params=payload)
            r.raise_for_status()

def get_movie_candidates(numberOfCandidates):
    payload = {'apiKey': radApiKey}
    r = requests.get("{}/movie".format(radarrApiUrl), params=payload)
    r.raise_for_status()
    data = r.json()
    moviesWithFiles = []

    for i in data:
        if i['hasFile'] == True:
            moviesWithFiles.append(i)

    sortedMovies = (sorted(moviesWithFiles, key = lambda i: i['movieFile']['dateAdded']))
    return sortedMovies[:numberOfCandidates]

diskFree = df_media_path()

if diskFree < lowDiskMark:
    if sonHost:
        print('Processing TV series')
        sonarrApiUrl = 'http://{}:{}/api/v3'.format(sonHost,sonPort)
        delete_old_tv(get_tv_candidates(3))
    else:
        print('sonarr host/key not defined.')

    if radHost:
        print('Processing Movies')
        radarrApiUrl = 'http://{}:{}/api/v3'.format(radHost,radPort)
        delete_old_movies(get_movie_candidates(3))
    else:
        print('radarr host/key not defined.')

else:
    print(diskFree,'GiB free. Nothing to do.')
