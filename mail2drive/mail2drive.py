#!/usr/bin/env python3

'''Add Gmail message attachments to a Google Driver folder'''

import os.path
from base64 import urlsafe_b64decode
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
import yaml

def main():
    '''Add Gmail message attachments to a Google Driver folder'''
    # pylint: disable=maybe-no-member
    def download_attachments(msg_id):
        message = gmail_service.users().messages().get(userId='me',id=msg_id).execute()
        for part in message['payload']['parts']:
            if part['filename'] and part['filename'] not in existing_files:
                if 'data' in part['body']:
                    data = part['body']['data']

                else:
                    att_id = part['body']['attachmentId']
                    att = gmail_service.users().messages().attachments().get(userId='me'
                                                            ,messageId=msg_id,id=att_id).execute()
                    data = att['data']

                file_data = urlsafe_b64decode(data.encode('UTF-8'))
                path = part['filename']
                print(f'downloading attachment "{path}"')
                upload_list.append(path)

                with open(path, 'wb') as file:
                    file.write(file_data)

    def upload_file(filename):
        file_metadata = {'name': filename,'parents': [folder_id]}
        media = MediaFileUpload(filename, resumable=True)
        file = drive_service.files().create(body=file_metadata,media_body=media,
                                                                            fields='id').execute()
        print(f'Uploading file "{filename}"')
        return file.get('id')

    try:
        script_path = os.path.dirname(os.path.realpath(__file__))
        config_path = f'{script_path}/config'
        auth_path = f'{script_path}/auth'
        with open(f'{config_path}/mappings.yml', 'r', encoding='UTF-8') as map_file:
            yaml_map = yaml.safe_load(map_file)

        # Begin OAuth2 flow
        print('Authenticating with Google APIs')
        scopes = ['https://www.googleapis.com/auth/gmail.readonly',
                'https://www.googleapis.com/auth/drive']
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is created
        # automatically when the authorization flow completes for the first time.
        if os.path.exists(f'{auth_path}/token.json'):
            creds = Credentials.from_authorized_user_file(f'{auth_path}/token.json', scopes)

        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                print('Refreshing OAuth token')
                creds.refresh(Request())

            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    f'{auth_path}/client_secrets.json', scopes)
                creds = flow.run_local_server(port=0)

            # Save the credentials for the next run
            with open(f'{auth_path}/token.json', 'w', encoding='UTF-8') as token:
                token.write(creds.to_json())
        # End OAuth2 flow

        # Build services for Gmail and Drive
        gmail_service = build('gmail', 'v1', credentials=creds)
        drive_service = build('drive', 'v3', credentials=creds)

        # Loop through process for each mail label : drive folder mapping
        mappings = yaml_map['mappings']
        for mapping in mappings:
            folder_id = mapping['folder']
            label_id = mapping['label']
            upload_list = []
            existing_files = []

            print(f'Processing {mappings.index(mapping) + 1} of {len(mappings)} {mapping}')

            print('Getting list of existing files in Drive folder')
            drive_results = drive_service.files().list(pageSize=1000,
                q=f"'{folder_id}' in parents and trashed=false",fields="files(name)").execute()
            drive_files = drive_results.get('files', [])
            for drive_file in drive_files:
                existing_files.append(drive_file['name'])

            print('Getting labelled messages')
            gmail_results = gmail_service.users().messages().list(userId='me',
                                                                labelIds=label_id).execute()
            messages = gmail_results.get('messages', [])

            if not messages:
                print('No messages found.')
                continue

            print('Downloading missing attachments')
            for message in messages:
                download_attachments(message['id'])

            if not upload_list:
                print('No missing attachments to download')
                continue

            print(f'Uploading missing attachments to Drive folder {folder_id}')
            for upload in upload_list:
                upload_file(upload)

            if upload_list:
                print('Cleaning up downloaded attachments')
                for upload in upload_list:
                    os.remove(upload)

    except HttpError as h_exc:
        print(f'A HTTP error has occurred.\n{h_exc}')
        raise h_exc

    except FileNotFoundError as f_exc:
        print(f'FileNotFoundError: {f_exc}')
        raise f_exc

    except yaml.YAMLError as y_exc:
        print(f'Error parsing mappings.yml.\n{y_exc}')
        raise y_exc

if __name__ == '__main__':
    main()
