#!/usr/bin/env python3

'''List Gmail label IDs and Drive folder IDs'''

import os.path
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

def main():
    '''List Gmail label IDs and Drive folder IDs'''
    # pylint: disable=maybe-no-member

    # Begin OAuth2 flow
    script_path = os.path.dirname(os.path.realpath(__file__))
    auth_path = f'{script_path}/auth'
    print('Authenticating with Google APIs')
    scopes = ['https://www.googleapis.com/auth/gmail.readonly',
            'https://www.googleapis.com/auth/drive']
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is created
    # automatically when the authorization flow completes for the first time.
    if os.path.exists(f'{auth_path}/token.json'):
        creds = Credentials.from_authorized_user_file(f'{auth_path}/token.json', scopes)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            print('Refreshing OAuth token')
            creds.refresh(Request())

        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                f'{auth_path}/client_secrets.json', scopes)
            creds = flow.run_local_server(port=0)

        # Save the credentials for the next run
        with open(f'{auth_path}/token.json', 'w', encoding="UTF-8") as token:
            token.write(creds.to_json())
    # End OAuth2 flow

    # Build services for Gmail and Drive
    try:
        gmail_service = build('gmail', 'v1', credentials=creds)
        drive_service = build('drive', 'v3', credentials=creds)

        print('Drive folder IDs:')
        drive_results = drive_service.files().list(pageSize=1000,fields='files(name, id)',
            q='mimeType="application/vnd.google-apps.folder" and trashed=false').execute()
        drive_folders = drive_results.get('files', [])
        for folder in drive_folders:
            print(f'name: "{folder["name"]}" id: {folder["id"]}')

        print('Gmail label IDs:')
        gmail_results = gmail_service.users().labels().list(userId='me').execute()
        gmail_labels = gmail_results.get('labels', [])
        for label in gmail_labels:
            print(f'name: "{label["name"]}" id: {label["id"]}')

    except HttpError as error:
        print(f'An error occurred: {error}')

if __name__ == '__main__':
    main()
